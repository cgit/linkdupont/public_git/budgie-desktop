Name:		budgie-desktop
Version:	10.2.9
Release:	2%{?dist}
Summary:	Modern desktop with a focus on simplicity and elegance

License:	GPLv2
URL:		https://solus-project.com/budgie/
Source0:	https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.xz
Source1:	20_com.solus-project.icon-tasklist.fedora-budgie-spin.gschema.override
Source2:	20_org.gnome.desktop.background.fedora-budgie-spin.gschema.override
Source3:	20_org.gnome.desktop.interface.fedora-budgie-spin.gschema.override
Source4:	20_org.gnome.desktop.wm.preferences.fedora-budgie-spin.gschema.override
Source5:	20_org.gnome.settings-daemon.plugins.xsettings.fedora-budgie-spin.gschema.override

Patch0:		budgie-desktop-10.2.8-fix-desktop-specifications.patch
Patch1:		budgie-desktop-10.2.8-dynamic-pkgconfig-paths.patch

BuildRequires:	pkgconfig(gobject-2.0) >= 2.44.0
BuildRequires:	pkgconfig(gio-2.0) >= 2.44.0
BuildRequires:	pkgconfig(gio-unix-2.0) >= 2.44.0
BuildRequires:	pkgconfig(gtk+-3.0) >= 3.16.0
BuildRequires:	pkgconfig(libpulse) >= 2.0
BuildRequires:	pkgconfig(libpulse-mainloop-glib) >= 2.0
BuildRequires:	pkgconfig(gobject-introspection-1.0) >= 1.44.0
BuildRequires:	pkgconfig(uuid)
BuildRequires:	pkgconfig(libpeas-gtk-1.0) >= 1.8.0
BuildRequires:	pkgconfig(libmutter) >= 3.16.0
BuildRequires:	pkgconfig(gnome-desktop-3.0) >= 3.16.0
BuildRequires:	pkgconfig(gsettings-desktop-schemas) >= 3.16.0
BuildRequires:	pkgconfig(ibus-1.0) >= 1.5.13
BuildRequires:	pkgconfig(libgnome-menu-3.0) >= 3.10.1
BuildRequires:	pkgconfig(polkit-gobject-1) >= 0.105
BuildRequires:	pkgconfig(polkit-agent-1) >= 0.105
BuildRequires:	pkgconfig(libwnck-3.0) >= 3.14.0
BuildRequires:	pkgconfig(accountsservice) >= 0.6.40
BuildRequires:	pkgconfig(upower-glib) >= 0.99.0
BuildRequires:	pkgconfig(gnome-bluetooth-1.0) >= 3.16.0
BuildRequires:	intltool >= 0.50.0
BuildRequires:	vala >= 0.28
BuildRequires:	gtk-doc
BuildRequires:	chrpath
BuildRequires:	sed
BuildRequires:	desktop-file-utils

Requires:	gnome-screensaver
Requires:	network-manager-applet
Requires:	google-roboto-fonts
Requires:	dmz-cursor-themes

%description
Budgie is a modern desktop environment that focuses on simplicity and elegance.
Written from scratch with integration in mind, the Budgie desktop tightly
integrates with the GNOME stack, employing underlying technologies to offer an
alternative desktop experience.


%package devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package docs
Summary:	Documentation for %{name}
BuildArch:	noarch

%description docs
%{summary}.


%prep
%autosetup -p0

%build
%configure --disable-static
%make_build


%install
%make_install
find %{buildroot} -name '*.la' -exec rm -f {} ';'
%find_lang %{name}
/usr/bin/desktop-file-validate %{buildroot}%{_sysconfdir}/xdg/autostart/*.desktop
/usr/bin/desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop

/usr/bin/chrpath --delete %{buildroot}%{_bindir}/budgie-{panel,daemon,polkit-dialog,run-dialog}
/usr/bin/chrpath --delete %{buildroot}%{_libdir}/libraven.so.0.0.0
/usr/bin/chrpath --delete %{buildroot}%{_libdir}/%{name}/plugins/*.so

install -Dm00644 %SOURCE1 %SOURCE2 %SOURCE3 %SOURCE4 %SOURCE5 %{buildroot}%{_datadir}/glib-2.0/schemas


%post
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/sbin/ldconfig

%postun
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
/sbin/ldconfig

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%license LICENSE
%doc README.md
%{_sysconfdir}/xdg/autostart/*.desktop
%{_bindir}/budgie-*
%{_libdir}/%{name}/plugins/*.{plugin,so}
%{_libdir}/girepository-1.0/Budgie-1.0.typelib
%{_libdir}/%{name}/Gvc-1.0.typelib
%{_libdir}/*.so.*
%{_datadir}/applications/*.desktop
%{_datadir}/glib-2.0/schemas
%{_datadir}/gnome-session/sessions/%{name}.session
%{_datadir}/icons/*
%{_datadir}/xsessions/%{name}.desktop

%files docs
%license LICENSE
%doc README.md
%{_datadir}/gtk-doc/*

%files devel
%license LICENSE
%doc README.md
%{_includedir}/*.h
%{_includedir}/%{name}/*.h
%{_libdir}/*.{so,la}
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/Budgie-1.0.gir
%{_datadir}/vala/vapi/budgie-1.0.*


%changelog
* Fri Feb 10 2017 Link Dupont <linkdupont@fedoraproject.org> - 10.2.9-2
- Add missing dependencies to cursor and fonts used in default schemas

* Tue Jan 10 2017 Link Dupont <linkdupont@fedoraproject.org> - 10.2.9-1
- New upstream version

* Sun Nov 27 2016 Link Dupont <linkdupont@fedoraproject.org> - 10.2.8-2
- Add Fedora-specific schema overrides

* Thu Nov 10 2016 Link Dupont <linkdupont@fedoraproject.org> - 10.2.8-1
- Initial release
